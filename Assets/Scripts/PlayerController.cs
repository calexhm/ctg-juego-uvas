﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocidadMov= 700f;
    public int limitChange = 50;

    void Update()
    {
        MovePlayer(); //Mueve al jugador,
  
       // IncreaseWeight(); //Verifica el "Peso" para cambiar la velocidad de movimiento.
    }

    void MovePlayer()
    {
        if (Input.GetKey("left"))
        {
           gameObject.transform.Translate(-velocidadMov * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey("right"))
        {
            gameObject.transform.Translate(velocidadMov * Time.deltaTime, 0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ScoreManager.scoreManager.ScoreControl(collision.tag);
    }



    // Gestiona la velocidad del jugador;
    /*void IncreaseWeight()
    {

        if (ScoreManager.score > limitChange && velocidadMov >= 300)
        {
            velocidadMov = velocidadMov - 100;

            limitChange += 50;
        }
        else

        if (ScoreManager.score < limitChange && velocidadMov < 700)
        {
            velocidadMov = velocidadMov + 100;

            limitChange -= 50;

        }
    }
    */
}
