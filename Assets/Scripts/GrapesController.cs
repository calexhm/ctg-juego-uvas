﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapesController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("LimitDown") || collision.tag.Equals("Player"))
        {
            Destroy(gameObject);
        }
    }


}
