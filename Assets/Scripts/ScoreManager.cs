﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public static ScoreManager scoreManager;

    public Text scoreText;
    public static int score = 0;

    public void Start()
    {
        scoreManager = this; 
    }


    //Aumenta o decrementa puntos segun el el tipo de objeto (Uva) recogido.
    public void ScoreControl(string tagCollision)
    {
        if (tagCollision.Equals("ClusterGrape"))
        {
            RaiseScore(1);
        }
        else if (tagCollision.Equals("ExtraPoints"))
        {
            RaiseScore(30);
        }
        else if (tagCollision.Equals("LostPoints"))
        {
           DecreaseScore(25);
        }
    }

    public void RaiseScore(int s)
    {
        score += s;
        scoreText.text = score + "";   
    }

    public void DecreaseScore(int s)
    {

        int result = score - s;

        if (result >= 0)
        {
            score = result;
            scoreText.text = score + "";
        }
        else if (result <= 0)
        {
            score = 0;
            scoreText.text = score + "";
        }

    }
}
