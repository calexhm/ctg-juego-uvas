﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrapeSpawner : MonoBehaviour
{

    private float timer;
    public GameObject grapePrefab;
    public GameObject parent;
    GameObject prefabGO;

    //Sprites tipos bonus
    Sprite extraPointsRacimo;
    Sprite lostPointsGreen;
    Sprite grapeImg;

    //Eventos
    public float maxTimeEvent; 
    private float decTimeEvent;
    string typeEvent = null;

    public static float timeGeneration = 1f;

    Vector3 newSizeImg = new Vector3(3, 3, 0);

    private void Start()
    {
        extraPointsRacimo = Resources.Load<Sprite>("Sprites/Racimo");
        lostPointsGreen = Resources.Load<Sprite>("Sprites/RacimoGreen");
        grapeImg = Resources.Load<Sprite>("Sprites/Grape");

        maxTimeEvent = 20f; //Duracion del evento
        decTimeEvent = 50f; // Disminucion del tiempo del evento
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= timeGeneration)
        {
          timer = 0f;

            if (typeEvent != null && maxTimeEvent > 0f)
            {
                StartEvent(typeEvent);
          
            }
            else
            {
                prefabGO = GrapeSpawn(false, false); //Crea objetos (uvas o racimos).
                timeGeneration = 1f;
                typeEvent = MakeEvent(); //Inicia eventos.
                maxTimeEvent = 20f;

            }
         
        }

    }

    GameObject GrapeSpawn(bool extraPoints, bool lostPoints)
    {

        bool genXtraPoints = extraPoints;
        bool genLostPoints = lostPoints;

        //Crea una nueva uva cada 2 segundos en una posicion aleatoria.
        float rangeX = Random.Range(-525f, 525f); //Posicion aleatoria.

        Vector3 position = new Vector3(rangeX, grapePrefab.transform.localPosition.y, 0);

        GameObject prefabGO = Instantiate(grapePrefab);


        prefabGO.gameObject.transform.SetParent(parent.transform); // aqui le indicamos quien es su padre para que se cree en la posicion de la jerarquia que queremos
                                                                   //prefabGO.transform.localPosition = grapePrefab.transform.localPosition; // indicamos la posicion del clon

        prefabGO.transform.localPosition = position; //Genera clones en una posicion X aleatoria.

        /*prefabGO =*/ EspecialObjects(prefabGO, genXtraPoints, genLostPoints);

        return prefabGO;
    }

    /*GameObject*/ void EspecialObjects(GameObject prefabGO, bool extraPoints, bool lostPoints )
    {
        //Genera Racimos para puntos extra
        float rangePoints = Random.Range(1, 25);

        //15 Creara un racimo puntos extras. 
        //rangePoints = 15;
        if (rangePoints == 15 || extraPoints)
        {

            prefabGO.transform.GetChild(0).GetComponent<Image>().sprite = extraPointsRacimo;

            prefabGO.transform.localScale = newSizeImg;

            prefabGO.transform.GetComponent<Rigidbody2D>().gravityScale = 50;

            prefabGO.tag = "ExtraPoints";

        } else
            if (rangePoints == 13 || lostPoints)
            {

            prefabGO.transform.GetChild(0).GetComponent<Image>().sprite = lostPointsGreen;

            prefabGO.transform.localScale = newSizeImg;

            prefabGO.transform.GetComponent<Rigidbody2D>().gravityScale = 50;

            prefabGO.tag = "LostPoints";
        }


       // return prefabGO;
    }


    string MakeEvent()
    {
        string typeEvent = null;

        int startEventGreen = Random.Range(1, 100);

        if(startEventGreen == 13) {
            typeEvent = "GreenRain";
        }

        int startEventPurple = Random.Range(1, 200);

        if (startEventPurple == 77)
        {
            typeEvent = "PurpleRain";
        }


        return typeEvent;
    }

    void StartEvent(string typeEvent)
    {

        if (typeEvent.Equals("GreenRain"))  //Crea solo racimos verdes.
        {
            prefabGO = GrapeSpawn(false, true);
            maxTimeEvent -= decTimeEvent * Time.deltaTime;
            timeGeneration = 0.2f;
        } else 
           
        if (typeEvent.Equals("PurpleRain")) //Crea solo racimos purpuras;
        {
            prefabGO = GrapeSpawn(true, false);
            maxTimeEvent -= decTimeEvent * Time.deltaTime;
            timeGeneration = 0.1f;
        }


    }

}
