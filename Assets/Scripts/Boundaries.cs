﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    public float x1 = 71.3f, x2 = 1030.3f;
    public float y1 = 42f, y2 = 118f;

    private void Update()
    {
        y1 = 42f;
        y2 = 118f;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, x1, x2),
            Mathf.Clamp(transform.position.y, y1, y2), transform.position.z);
    }

}
